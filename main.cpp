
#include "namemap.hh"

#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>

using std::cerr;
using std::cin;
using std::cout;
using std::endl;
using std::getline;
using std::ifstream;
using std::istream;
using std::string;

void usage_and_exit(char**argv, int exit_code) {
  cerr << argv[0] << ": Usage: " << argv[0] << " <model_file.mzn> <paths_file.paths> [log_file.log]\n\n"
    << "  --no-expand\n"
    << "      Disable expression expansion (.mzn file is not required)\n"
    << "  --add-quotes\n"
    << "      Place quotes around renamed variables (for re-solving\n"
    << "        fzn as renamed mzn). Note that names are not escaped.\n"
    << "  --show-decomp-callsite\n"
    << "      Print last expression in user's model before decomposition.\n"
    << "        This can be useful for element constraints however it can also be misleading\n"
    << "  --help\n"
    << "      This text.\n"
    << "\n";

  exit(exit_code);
}

int main(int argc, char**argv) {
  if(argc < 3) {
    cerr << "Not enough arguments.\n";
    usage_and_exit(argv, EXIT_FAILURE);
  }

  string model_path;
  string paths_path;
  string log_path;

  bool show_callsite = false;
  bool expand_expressions = true;
  bool add_quotes = false;

  int i = 1;
  while(i < argc) {
    if(strcmp(argv[i], "--no-expand") == 0) {
      expand_expressions = false;
    } else if(strcmp(argv[i], "--add-quotes") == 0) {
      add_quotes = true;
    } else if(strcmp(argv[i], "--show-decomp-callsite") == 0) {
      show_callsite = true;
    } else if(strcmp(argv[i], "--help") == 0) {
      usage_and_exit(argv, EXIT_SUCCESS);
    } else {
      string p = argv[i];
      if(p.size() > 3) {
        string ext = p.substr(p.size()-3, 3);
        if(ext == "mzn") {
          model_path = p;
        } else if(ext == "ths") {
          paths_path = p;
        } else if(ext == "log") {
          log_path = p;
        } else {
          cerr << "Unknown ext: " << ext << " from " << p << "\n";
          usage_and_exit(argv, EXIT_FAILURE);
        }

      } else {
        cerr << "Unknown arg: " << p << "\n";
        usage_and_exit(argv, EXIT_FAILURE);
      }
    }

    i++;
  }

  NameMap nm(paths_path, model_path, expand_expressions, add_quotes, show_callsite);
  istream* ifs = nullptr;

  if(log_path.empty()) { // read from stdin
    ifs = &cin;
  } else {
    ifstream* is = new ifstream(log_path);
    if(!is->is_open()) {
      cerr << "Cannot open: " << log_path << "\n";
      exit(EXIT_FAILURE);
    }
    ifs = is;
  }

  string line;
  while(getline(*ifs, line)) {
    cout << nm.replaceNames(line) << endl;
  }

  if(!log_path.empty()) delete ifs;
  return EXIT_SUCCESS;
}
